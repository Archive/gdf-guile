/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
#include <main.h>

#include "debugger-program-impl.c"

void
scm_debug_print (SCM obj)
{
    scm_display (obj, scm_current_output_port ());
    scm_newline (scm_current_output_port ());
    scm_flush (scm_current_output_port ());
}

GNOME_Debugger_Program
gdf_guile_program_module_init (void)
{
    CORBA_ORB orb;
    CORBA_Environment ev;
    GNOME_Debugger_Program server;

    PortableServer_POA root_poa;
    PortableServer_POAManager pm;

    /* Setup CORBA */
    CORBA_exception_init (&ev);
    
    orb = gnome_CORBA_ORB ();

    root_poa = 
        (PortableServer_POA) CORBA_ORB_resolve_initial_references (orb,
                                                                   "RootPOA",
                                                                   &ev);
    
    server = impl_GNOME_Debugger_Program__create (root_poa, &ev);
    pm = PortableServer_POA__get_the_POAManager (root_poa, &ev);
    PortableServer_POAManager_activate (pm, &ev);

    goad_server_register (CORBA_OBJECT_NIL, server, NULL, NULL, &ev);

    CORBA_exception_free (&ev);

    return server;
}

void
scm_init_gdf_guile_program_module (void)
{
#ifndef SCM_MAGIC_SNARFER
#include "main.x"
#endif

    scm_init_gdf_guile_program_glue ();
}
