(define-module (gnome debugger commander gdf-guile)
  :use-module (gtk dynlink)
  :use-module (gnome gnorba)
  :use-module (gnome debugger))

(merge-compiled-code "scm_init_gdf_guile_commander_module" "libgdf-guile-commander-module")

