/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
#include <libguile.h>
#include <guile-gtk.h>
#include <gnome-debug/gnome-debug.h>
#include "main.h"

void *
_corba_object_copy (void *object)
{
    fprintf (stderr, "corba_object_copy (%p)\n", object);
    ORBIT_ROOT_OBJECT_REF (object);
    return object;
}

void
_corba_object_free (void *object)
{
    fprintf (stderr, "corba_object_free (%p)\n", object);
    ORBIT_ROOT_OBJECT_UNREF (object);
}

void
gdf_guile_commander_main_iteration (void)
{
    gtk_main_iteration_do (FALSE);
}

void
gdf_guile_commander_main (void)
{
    gtk_main ();
}
