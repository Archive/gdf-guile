/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
#ifndef MAIN_H
#define MAIN_H 1

#include <gnome.h>
#include <config.h>
#include <libgnorba/gnorba.h>

#include <guile/gh.h>
#include <libguile/dynl.h>
#include <libguile/snarf.h>
#include <libguile/modules.h>
#include <libguile/dynwind.h>
#include <libguile/strports.h>

#include <gnome-debug/gnome-debug.h>

/*** App-specific servant structures ***/

typedef struct {
    POA_GNOME_Debugger_Program servant;
    PortableServer_POA poa;

    int refcount;
} impl_POA_GNOME_Debugger_Program;

/* main.c */
extern void scm_debug_print (SCM);
extern void scm_init_gdf_guile_program_module (void);
extern GNOME_Debugger_Program gdf_guile_program_module_init (void);

/* gdf-guile-glue.c */
extern void scm_init_gdf_guile_program_glue ();


#endif /* MAIN_H */
